import { useState, useContext, createContext } from "react";

const LoadingContext = createContext();

const LoadingProvider = ({ children }) => {
  const [load, setLoad] = useState(false);

  return (
    <LoadingContext.Provider value={[load, setLoad]}>
      {children}
    </LoadingContext.Provider>
  );
};

const useLoad = () => useContext(LoadingContext);

export { useLoad, LoadingProvider };
